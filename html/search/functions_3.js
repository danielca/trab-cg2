var searchData=
[
  ['dist',['dist',['../classgeometry_1_1_point.html#a8da689422b3b004369e1467f8123e8a7',1,'geometry::Point']]],
  ['distance',['distance',['../classgeometry_1_1_line.html#ac6b10e2377ad195ca3c2a6148b1aa770',1,'geometry.Line.distance()'],['../namespacegeometry.html#a9453ec6bad9d4ba50724a4df0b1a74ed',1,'geometry.distance()']]],
  ['distancetoline',['distanceToLine',['../namespacegeometry.html#a96071e09708da91677cc86ea91705f5e',1,'geometry']]],
  ['doeslinecrosspolygon',['doesLineCrossPolygon',['../classgeometry_1_1_polygon.html#ae4ab82f837783f51b39a41427e46f5df',1,'geometry::Polygon']]],
  ['dot',['dot',['../namespacematrix.html#a4023520a6d24b4de107acd3c284db948',1,'matrix']]],
  ['dotprod',['dotProd',['../classgeometry_1_1_point.html#a4df6e13ff003843f967090fb7f51cc85',1,'geometry::Point']]]
];
