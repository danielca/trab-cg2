var searchData=
[
  ['translate',['translate',['../namespacematrix.html#a74da677cb3d0158ed4a2d79d15cfc478',1,'matrix']]],
  ['translateandrotate',['translateAndRotate',['../namespacematrix.html#ac41a59bd795819b4d4ac63531c08371a',1,'matrix']]],
  ['translateandtransform',['translateAndTransform',['../namespacematrix.html#a124af7b52efe8716b3a20db8204f3530',1,'matrix']]],
  ['translatepolygon',['translatePolygon',['../namespacemain.html#aca61579c6f514f7bae04149cb1ac80b6',1,'main']]],
  ['triangle',['Triangle',['../classgeometry_1_1_triangle.html',1,'geometry']]],
  ['tripleprod',['tripleProd',['../classgeometry_1_1_point.html#acce6c690969f757d26e4a544955cb8f4',1,'geometry::Point']]],
  ['tx',['tx',['../classgeometry_1_1_box.html#a0e744b5244823afefa01b40c2613037f',1,'geometry::Box']]],
  ['ty',['ty',['../classgeometry_1_1_box.html#aca0410b99a6d8b59060f6fcbf12db160',1,'geometry::Box']]]
];
