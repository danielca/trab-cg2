var searchData=
[
  ['cancelpolygon',['cancelPolygon',['../namespacemain.html#a9641b8c073c7079e597f39b5f7f6cf0d',1,'main']]],
  ['canrotate',['canRotate',['../namespacefunctions.html#a294f1feffaeb1fb143a533860120b6c6',1,'functions']]],
  ['cantranslate',['canTranslate',['../namespacefunctions.html#af4d8f05e212cb33f0a5d4f1362c5a5f5',1,'functions']]],
  ['ccw',['ccw',['../classgeometry_1_1_polygon.html#a4c6914db65978bbd2232f28bbb41b69b',1,'geometry.Polygon.ccw()'],['../namespacegeometry.html#ad67511f31b70990660efd63da577f482',1,'geometry.ccw()']]],
  ['ccw3',['ccw3',['../namespacegeometry.html#a826edf3113b2277f596bc7927da2fd4e',1,'geometry']]],
  ['centre',['centre',['../classgeometry_1_1_box.html#aaab7456e78cc5b951e1b832b6816d332',1,'geometry::Box']]],
  ['changesize',['changeSize',['../namespacemain.html#a526cfe84f4e80095febf0d3f4b7b5358',1,'main']]],
  ['close',['close',['../classgeometry_1_1_point.html#ab9ec6d1fd080ba8b7eb99e5832505c85',1,'geometry.Point.close()'],['../namespacegeometry.html#a118331eb1c38459dd57dad62f75abea5',1,'geometry.close()']]],
  ['coloredpolygon',['ColoredPolygon',['../classclasses_1_1_colored_polygon.html',1,'classes']]],
  ['compnormal',['compNormal',['../classgeometry_1_1_polygon.html#aa39e7d0353ffad956679321aca725d35',1,'geometry::Polygon']]],
  ['contains',['contains',['../classgeometry_1_1_polygon.html#a64880abb26797d5ad564a6f0399b216c',1,'geometry.Polygon.contains()'],['../classgeometry_1_1_box.html#af184892d7008637d2d34a9cc4fa4fdd5',1,'geometry.Box.contains()']]],
  ['contains2',['contains2',['../classgeometry_1_1_box.html#af22304a33ca2c58b7ca21427608602e0',1,'geometry::Box']]],
  ['crossprod',['crossProd',['../classgeometry_1_1_point.html#aa9a3448e0da24f3132a4ccc7abf191a7',1,'geometry::Point']]],
  ['crossprod2d',['crossProd2d',['../classgeometry_1_1_point.html#ae6747fdae2c6ec09961298171862ef4b',1,'geometry::Point']]]
];
