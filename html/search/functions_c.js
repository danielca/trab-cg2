var searchData=
[
  ['scale',['scale',['../namespacematrix.html#a3809ea999967baf83175d262fe553d2d',1,'matrix']]],
  ['setcolor',['setColor',['../classclasses_1_1_colored_polygon.html#a69b04b4e499533512f4b7f9914d8aa70',1,'classes::ColoredPolygon']]],
  ['setparameters',['setParameters',['../classgeometry_1_1_box.html#a2af13fbaf9c38d904e05d73e35b5f9f9',1,'geometry::Box']]],
  ['shortestpathtoline',['shortestPathToLine',['../namespacegeometry.html#acaccff5b694d39c9db5072c671c6568b',1,'geometry']]],
  ['sqrdist',['sqrDist',['../classgeometry_1_1_point.html#a28f393df5b0c4a297157a3ae55f1f4a3',1,'geometry::Point']]]
];
