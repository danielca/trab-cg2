## @package functions
# File where created classes are
#
# @author Daniel Assumpcao
#
from geometry import *

## Class for a degenerated line, used to draw the temporary line on the construction of a new polygon
#
class DegLine(object):
	def __init__(self, pointA):
		## The constructor
		self.pointA = pointA
		self.pointB = Point(pointA.x, pointA.y)
## Extension class from geometry.polygon
#
## Added color and necessary atributes to make the hierarchy viable
#
class ColoredPolygon(Polygon):
	## The constructor
	def __init__(self, points, color):
		super(ColoredPolygon, self).__init__(points)
		self.r = color[0]
		self.g = color[1]
		self.b = color[2]
		self.parents = []
		self.children = []
		self.nails = []
	## Sets the polygon color
	# @param self The ColoredPolygon object
	# @param r The red parameter of RGB
	# @param g The green parameter of RGB
	# @param b The blue parameter of RGB
	# 
	def setColor(self, r, g, b):
		self.r = r
		self.g = g
		self.b = b
##Class made for the implementation of the double click
#
class DoubleClick(object):
	## The constructor
	def __init__(self, time):
		self.time = time
	## Checks the time difference between clicks to determine if it is double click
	def isDoubleClicked(self, finalTime):
		return finalTime - self.time < 0.2
