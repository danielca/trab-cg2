## @package main
# File where all implementation are
#
# @author Daniel Assumpcao
#


from __future__ import division
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

from geometry import *
from matrix import *
from classes import *
from triangulate import *
from functions import *

import sys
import math
from time import time

screenW = 800 
screenH = 600

tempLine = DegLine(Point(0,0))
clicked = False
selectedPolygon = None
activePolygon = []
allNails = []
allPolygons = []
allPoints = []
doubleClick = DoubleClick(time())
transformedChildren = []
transformedNails = []

## Main function
#
def main():
	glutInit(sys.argv)
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA)
	glutInitWindowPosition(100,100)
	glutInitWindowSize(screenW,screenH)
	glutCreateWindow("Trabalho 2 - CG - Daniel Assumpcao")
	glutMouseFunc(getMouse)
	glutPassiveMotionFunc(mouseDrag)
	glutMotionFunc(mouseMove)
	glutDisplayFunc(renderScene)
	glutIdleFunc(renderScene)
	glutReshapeFunc(changeSize)
	glutMainLoop()

## Function to resize the window
# @param w The screen width
# @param h The screen height
#

def changeSize(w, h):
	global screenW
	global screenH

	# Prevent a divide by zero, when window is too short
	# (you cant make a window of zero width).
	if(h == 0):
	       h = 1

	ratio = 1.0* w / h

	# Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()
	

	# Set the viewport to be the entire window
	screenW = w
	screenH = h
	glViewport(0, 0, w, h)

## Callback function used to monitor the mouse motion while clicked
# @param x The x coordinate of the mouse
# @param y The y coordinate of the mouse
#
def mouseMove(x,y):
	global startedPoint
	global transformedNails
	global transformedChildren

	actualPoint = Point(x,y)
	if(selectedPolygon is not None):
		if(selectedPolygon.parents and len(selectedPolygon.nails) == 1):
			rotatePolygon(actualPoint)
		elif(not selectedPolygon.parents):
			translatePolygon(selectedPolygon, actualPoint)
	transformedChildren = []
	transformedNails = []

## Function that applies a transformation matrix to a given polygon children
# @param polygon The parent polygon
# @param matrix The transformation matrix being applied
#
def applyTransformationToChildren(polygon,matrix):
	for child in set(polygon.children):
		if(child not in transformedChildren):
			transformedChildren.append(child)
			applyTransformationToPoints(child,matrix)
			if(child.children):
				applyTransformationToChildren(child,matrix)

##Function that rotate the selected polygon through a given point
# @param actualPoint The point which will be used as the rotation axis

def rotatePolygon(actualPoint):
	global startedPoint
	rotatePoint = selectedPolygon.nails[0]
	 

	startPoint = startedPoint - rotatePoint
	anglePoint = actualPoint - rotatePoint

	inner_product = startPoint.x*anglePoint.x + startPoint.y*anglePoint.y

	len1 = math.hypot(startPoint.x, startPoint.y)
	len2 = math.hypot(anglePoint.x, anglePoint.y)

	angle = math.acos(inner_product/(len1*len2))

	angle = angle*180/math.pi
	angle = math.copysign(angle, anglePoint.crossProd(startPoint).z)
	angle *= -1

	matrix = translateAndRotate(angle, rotatePoint, Point(0,0,1))
	applyTransformationToPoints(selectedPolygon, matrix)
	if(selectedPolygon.children):
		applyTransformationToChildren(selectedPolygon, matrix)
	startedPoint = actualPoint


## Function that moves the polygon to a given point
# @param polygon The polygon that will be moved
# @param actualPoint The point used to move the polygon
#
def translatePolygon(polygon, actualPoint):
	global startedPoint
	global transformedChildren
	distX = actualPoint.x - startedPoint.x
	distY = actualPoint.y - startedPoint.y
	matrix = translate(distX,distY,0)
	if(polygon.children):
		applyTransformationToChildren(polygon, matrix)
	applyTransformationToPoints(polygon, matrix)
	startedPoint = actualPoint



## Function that applies a transformation matrix to a given polygon points and nails
# @param polygon The parent polygon
# @param matrix The transformation matrix being applied
#
def applyTransformationToPoints(polygon,matrix):
	global transformedNails
	matrix = np.array(matrix)
	for point in polygon.points:
		pointN = np.array([point.x,point.y,0,1])
		
		result = matrix.dot(pointN)
		point.x = result[0]
		point.y = result[1]

	for nail in polygon.nails:
		if(nail not in transformedNails):
			transformedNails.append(nail)
			pointN = np.array([nail.x,nail.y,0,1])
			result = matrix.dot(pointN)
			nail.x = result[0]
			nail.y = result[1]

## Get the position of the mouse.
# @param x The x coordinate of the mouse
# @param y The y coordinate of the mouse
#
def mouseDrag(x, y):
	actualPoint = Point(x,y)
	if(clicked):
		tempLine.pointB = actualPoint
	glutPostRedisplay()

## Get the position and events of the mouse. This function creates the polygons and nails.
# @param button Which button is receiving the event
# @param state Which event is occurring with the button
# @param x The x coordinate of the mouse
# @param y The y coordinate of the mouse
#
def getMouse(button, state, x, y):
	global activePolygon
	global allPoints
	global clicked
	global tempLine
	global selectedPolygon
	global startedPoint
	global doubleClick
	if (button == GLUT_LEFT_BUTTON and state == GLUT_DOWN):
		actualPoint = Point(x,y)
		if(doubleClick.isDoubleClicked(time())):
			cancelPolygon()
			firstPolygon = True
			children = []
			parent = None
			removeNail = None 
			for nail in allNails:
				if nail.dist(actualPoint) <= 15:
					removeNail = nail

			for polygon in allPolygons:
				if(polygon.contains(actualPoint)):
					if(firstPolygon):
						parent = polygon
						nail = actualPoint
						firstPolygon = False
					else:
						if (polygon not in children):
							children.append(polygon)
				if(children):
					if(removeNail):
						if(removeNail in allNails):
							allNails.remove(removeNail)
						for child in children:
							if(child in parent.children):
								parent.children.remove(child)
							if (parent in child.parents):
								child.parents.remove(parent)
							try:
								child.nails.remove(removeNail)
							except ValueError:
								pass
					else:
						if polygon.parents:
							continue
						allNails.append(nail)
						parent.children += children
						for child in children:
							if(parent not in child.parents):
								child.parents.append(parent)
								child.nails.append(nail)

			
		else:
			doubleClick = DoubleClick(time())
			for polygon in reversed(allPolygons):
				if(polygon.contains(actualPoint)):
					selectedPolygon = polygon
					break
			if selectedPolygon and not activePolygon:
				startedPoint = actualPoint

			else:
				selectedPolygon = None
				clicked = True
				tempLine = DegLine(actualPoint)
				activePolygon.append(actualPoint)
				allPoints.append(actualPoint)

				if len(activePolygon) > 2 and activePolygon[-1].dist(activePolygon[0]) <= 15:
					del allPoints[-1]
					del activePolygon[-1]
					clicked = False
					tempLine = DegLine(Point(0,0))
					poly = ColoredPolygon(activePolygon[:],generateColor())
					allPolygons.append(poly)
					del activePolygon[:]
				if len(activePolygon) > 2:
					lastPoint = activePolygon[-2]
					line = Line(actualPoint, lastPoint)
					previousPoint = None
					for point in activePolygon:
						if previousPoint is None:
							previousPoint = point
							continue
						if point == lastPoint:
							break
						testLine = Line(point, previousPoint)
						previousPoint = point
						if intersects(testLine, line):
							for point in activePolygon:
								allPoints.remove(point) 
							del activePolygon[:]
							clicked = False
							tempLine = DegLine(Point(0,0))
	if (button == GLUT_LEFT_BUTTON and state == GLUT_UP):
		selectedPolygon = None

	if button == GLUT_RIGHT_BUTTON and state == GLUT_DOWN:
		cancelPolygon()


## Function that cancels the polygon that is being currently drawn
#
def cancelPolygon():
	global clicked
	global tempLine
	for point in activePolygon:
		allPoints.remove(point) 
	del activePolygon[:]
	clicked = False
	tempLine = DegLine(Point(0,0))



## Render the scene, drawing the polygons and nails.
#
def renderScene():
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	glPushMatrix()
	glMatrixMode (GL_PROJECTION)
	gluOrtho2D (0.0, screenW, screenH, 0.0)

	for polygon in allPolygons:
		glCallList(tessellate(polygon))
		for nail in polygon.nails:
			glPointSize(10.0)
			glBegin(GL_POINTS)
			glColor3f(255,255,255)
			glVertex3f(nail.x, nail.y, 0.0)
			glEnd()

	glPointSize(8.0)
	if(activePolygon):
		glBegin(GL_POINTS)
		glColor3f(255.0, 0.0, 0.0)
		glVertex3f(activePolygon[0].x, activePolygon[0].y, 0.0)
		glEnd()
	for i in range(1, len(activePolygon)):
		glBegin(GL_POINTS)
		glColor3f(255.0, 0.0, 0.0)
		glVertex3f(activePolygon[i].x, activePolygon[i].y, 0.0)
		glEnd()


		glLineWidth(3.0)
		glBegin(GL_LINES)
		glColor3f(255.0, 255.0, 255.0)
		glVertex3f(activePolygon[i-1].x, activePolygon[i-1].y, 0.0)
		glVertex3f(activePolygon[i].x, activePolygon[i].y, 0.0)
		glEnd()

	
	glLineWidth(3.0)
	glBegin(GL_LINES)
	glColor3f(255.0, 255.0, 255.0)
	glVertex3f(tempLine.pointA.x, tempLine.pointA.y, 0.0)
	glVertex3f(tempLine.pointB.x, tempLine.pointB.y, 0.0)
	glEnd()

	glPopMatrix()
	glutSwapBuffers()
	glutPostRedisplay()
	glFlush()

			

if __name__ == '__main__': main()