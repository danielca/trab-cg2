## @package functions
# File where created support functions are
#
# @author Daniel Assumpcao
#

from __future__ import division
from random import uniform

## Function that returns if a polygon can be moved
# @param polygon The polygon being tested
# @param children The list of polygons that are children of the tested polygon
# @return Returns True if the polygon can be translated, and False otherwise
#
def canTranslate(polygon, children):
	for child in children:
		for parent in child.parents:
			if allPolygons.index(polygon) > allPolygons.index(parent) or len(child.parents) >= 2:
				return False
		if(child.children):
			return canTranslate(polygon, child.children)
	return True

## Function that returns if a polygon can rotate
# @param polygon The polygon being tested
# @return Returns True if the polygon can be rotated, and False otherwise
#
def canRotate(polygon):
	for child in polygon.children:
		if len(child.parents) >= 2:
			return False
		elif(child.children):
			return canRotate(child)
	return True



## Functions that generates the polygon color and prevents it from being the background color or the nail color
#
def generateColor():
	r = uniform(0,1)
	g = uniform(0,1)
	b = uniform(0,1)
	if((r == 0 and g == 0 and b == 0) or (r == 1 and g == 1 and b == 1)):
		return generateColor()
	return [r,g,b]

## Function that checks if two lines intersect eachother
#
# This function is used to cancel the drawing of autointersecting polygons
# @param line1 A line
# @param line2 A line
#
def intersects(line1, line2):
	r1 = [(line1.p2.x - line1.p1.x), (line1.p2.y - line1.p1.y)]
	r2 = [(line2.p2.x - line2.p1.x), (line2.p2.y - line2.p1.y)]
	w1 = [(line2.p1.x - line1.p1.x), (line2.p1.y - line1.p1.y)]
	r1Xr2 = r1[0]*r2[1] - r1[1]*r2[0]
	if (abs(r1Xr2) > 0):
		w1Xr2 =  w1[0]*r2[1] - w1[1]*r2[0]
		w1Xr1 =  w1[0]*r1[1] - w1[1]*r1[0]
		t = w1Xr2/r1Xr2
		s = w1Xr1/r1Xr2
		if ( ((t>=0) and (t<=1)) and ((s>=0) and (s<=1)) ):
			return True
	return False